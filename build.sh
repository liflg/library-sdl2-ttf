#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        export CFLAGS="-ggdb"
        export CXXFLAGS="-ggdb"
    fi

    if [[ ( -z "$OPTIMIZATION" ) || ( "$OPTIMIZATION" -eq 2 ) ]]; then
        export CFLAGS="$CFLAGS -O2"
    else
        echo "Optimization level $OPTIMIZATION is not yet implemented!"
        exit 1
    fi

    mkdir -p "$BUILDDIR"/bin
    rm -rf "$PREFIXDIR"

cat << EOF > $BUILDDIR/bin/freetype-config
    #!/bin/bash
    for arg in "\$@"
    do
        case "\$arg" in
            "--cflags" )
               echo -I${PWD}/../library-freetype/${MULTIARCHNAME}/include/freetype2;;
            "--libs" )
               echo -L${PWD}/../library-freetype/${MULTIARCHNAME}/lib -lfreetype;;
            "--version" )
               # from source/docs/VERSION.DLL
               echo 18.3.12;;
            *)
               echo ERROR: \$arg is not supported;;
       esac
    done
EOF

    chmod 755 "$BUILDDIR"/bin/freetype-config

    ( cd source
      rm -rf external
      touch configure.ac aclocal.m4 configure Makefile.am Makefile.in)

    ( SDL2DIR="${PWD}/../library-sdl2/${MULTIARCHNAME}"
      export SDL_CFLAGS="-I${SDL2DIR}/include/SDL2 -D_REENTRANT"
      export SDL_LIBS="-L${SDL2DIR}/lib -lSDL2 -lpthread"
      cd "$BUILDDIR"
      ../source/configure \
         --prefix="$PREFIXDIR" \
         --with-freetype-exec-prefix="$PWD" \
         --disable-static
      make -j "$(nproc)"
      make install
      rm -rf "$PREFIXDIR"/{lib/*.la,lib/pkgconfig})

    # clean up afterwards
    ( cd source
      git clean -df .
      git checkout .)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/COPYING.txt "$PREFIXDIR"/lib/LICENSE-sdl2-ttf.txt

rm -rf "$BUILDDIR"

echo "SDL2_ttf for $MULTIARCHNAME is ready."
