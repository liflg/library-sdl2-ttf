Website
=======
https://www.libsdl.org/projects/SDL_ttf/

License
=======
zlib license (see the file source/COPYING.txt)

Version
=======
2.0.14

Source
======
SDL2_ttf-2.0.14.tar.gz (sha256: 34db5e20bcf64e7071fe9ae25acaa7d72bdc4f11ab3ce59acc768ab62fe39276)

Requires
========
* Freetype
* SDL2